﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Proje
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
       
        }
        SqlConnection bagla = new SqlConnection("Data Source=DESKTOP-AT1L9J0;Initial Catalog=Proje;Integrated Security=True");
        private void Form3_Load(object sender, EventArgs e)
        {
            bagla.Open();
            SqlCommand komut = new SqlCommand("Select Firma,Uretilenürün,Iskartasayısı From Proje1", bagla);
            SqlDataReader oku = komut.ExecuteReader();

            while(oku.Read())
            {
                chart1.Series["Üretilen Ürün"].Points.AddXY(oku[0].ToString(), oku[1].ToString());
                chart1.Series["Iskarta"].Points.AddXY(oku[0].ToString(), oku[2].ToString());
            }
            bagla.Close();
        }
    }
}
